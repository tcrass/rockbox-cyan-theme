#!/bin/bash

echo Creating backdrop...

convert ../common/src/default_blue.jpg \
  -rotate 90 -scale 240x320! \
  -modulate 175,90,90 \
  \( \
    -size 240x320 xc:#ffffff00 \
  \) -compose Screen -composite \
  cyan.bmp
