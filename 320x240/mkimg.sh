#!/bin/bash

echo Creating backdrop...

convert ../common/src/default_blue.jpg \
  -scale 320x240! \
  -modulate 175,90,90 \
  \( \
    -size 320x240 xc:#ffffff00 \
  \) -compose Screen -composite \
  cyan.bmp
